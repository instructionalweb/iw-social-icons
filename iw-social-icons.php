<?php
/*
Plugin Name: IW Social Icons
Plugin URI: https://instruction.austincc.edu/
Description: Add Insturctional Web social media icons.
Version: 1.01
Author: Shea Scott
Author URI: 
License: GPL2
*/

$iwPluginsURI = plugins_url('/iw-social-icons/');
$facebookSVG = $iwPluginsURI . 'icons/facebook.svg';
$instagramSVG = $iwPluginsURI . 'icons/instagram.svg';
$twitterSVG = $iwPluginsURI . 'icons/twitter.svg';
$linkedinSVG = $iwPluginsURI . 'icons/linkedin.svg';
$youtubeSVG = $iwPluginsURI . 'icons/youtube.svg';
$emailSVG = $iwPluginsURI . 'icons/email.svg';

// The widget class
class IW_Social_Icons_Widget extends WP_Widget {

	// Main constructor
	public function __construct() {
        parent::__construct(
            'iw_social_icons_widget',
            __( 'IW Social Icons', 'text_domain' ),
            array(
                'customize_selective_refresh' => true,
            )
        );
	}

	// The widget form (for the backend )
	public function form( $instance ) {	
   
        global $iwPluginsURI;
        global $facebookSVG;
        global $twitterSVG;
        global $instagramSVG;
        global $linkedinSVG;
        global $youtubeSVG;
        global $emailSVG;

        wp_register_style( 'iw_admin_css', $iwPluginsURI . 'css/admin-styles.css', false, '1.0' );
        wp_enqueue_style( 'iw_admin_css' );

        // Set widget defaults
        $defaults = array(
            'iw_title'    => 'Follow Us',
            'facebook'     => '',
            'twitter'     => '',
            'instagram'     => '',
            'linkedin'     => '',
            'youtube'     => '',
            'email'     => '',
            'whiteicons'     => '',
            'container_width'     => '100%',
        );
        
        // Parse current settings with defaults
        extract( wp_parse_args( ( array ) $instance, $defaults ) ); ?>

        <p>Add social url to include it in the widget.</p>

        <?php // whiteicons ?>
        <p>
            <input id="<?php echo esc_attr( $this->get_field_id( 'whiteicons' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'whiteicons' ) ); ?>" type="checkbox" value="1" <?php checked( '1', $whiteicons ); ?> />
            <label for="<?php echo esc_attr( $this->get_field_id( 'whiteicons' ) ); ?>"><?php _e( 'White Icons', 'text_domain' ); ?></label>
        </p>

        <?php // Container Width ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'container_width' ) ); ?>"><?php _e( 'Container Width (determines the size of the icons)', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'container_width' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'container_width' ) ); ?>" type="text" value="<?php echo esc_attr( $container_width ); ?>" />
        </p>

        <?php // Widget Title ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'iw_title' ) ); ?>"><?php _e( 'Title', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'iw_title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'iw_title' ) ); ?>" type="text" value="<?php echo esc_attr( $iw_title ); ?>" />
	    </p>

        <?php // Facebook Field ?>
        <p class="icon-wrapper">
            <label for="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ); ?>"><?php include('icons/facebook.svg') ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'facebook' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'facebook' ) ); ?>" type="text" value="<?php echo esc_attr( $facebook ); ?>" />
        </p>

        <?php // Twitter Field ?>
        <p class="icon-wrapper">
            <label for="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ); ?>"><?php include('icons/twitter.svg') ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'twitter' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'twitter' ) ); ?>" type="text" value="<?php echo esc_attr( $twitter ); ?>" />
        </p>

        <?php // Instagram Field ?>
        <p class="icon-wrapper">
            <label for="<?php echo esc_attr( $this->get_field_id( 'instagram' ) ); ?>"><?php include('icons/instagram.svg') ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'instagram' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'instagram' ) ); ?>" type="text" value="<?php echo esc_attr( $instagram ); ?>" />
        </p>
        
        <?php // Youtube Field ?>
        <p class="icon-wrapper">
            <label for="<?php echo esc_attr( $this->get_field_id( 'youtube' ) ); ?>"><?php include('icons/youtube.svg')  ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'youtube' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'youtube' ) ); ?>" type="text" value="<?php echo esc_attr( $youtube ); ?>" />
        </p>

        <?php // LinkedIn Field ?>
        <p class="icon-wrapper">
            <label for="<?php echo esc_attr( $this->get_field_id( 'linkedin' ) ); ?>"><?php include('icons/linkedin.svg')  ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'linkedin' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'linkedin' ) ); ?>" type="text" value="<?php echo esc_attr( $linkedin ); ?>" />
        </p>
        <?php // Email Field ?>
        <p class="icon-wrapper">
            <label for="<?php echo esc_attr( $this->get_field_id( 'email' ) ); ?>"><?php include('icons/email.svg')  ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'email' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'email' ) ); ?>" type="text" value="<?php echo esc_attr( $email ); ?>" />
        </p>

<?php }

	// Update widget settings
	public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['iw_title']    = isset( $new_instance['iw_title'] ) ? wp_strip_all_tags( $new_instance['iw_title'] ) : '';
        $instance['whiteicons'] = isset( $new_instance['whiteicons'] ) ? 1 : false;
        $instance['facebook']     = isset( $new_instance['facebook'] ) ? wp_strip_all_tags( $new_instance['facebook'] ) : '';
        $instance['twitter']     = isset( $new_instance['twitter'] ) ? wp_strip_all_tags( $new_instance['twitter'] ) : '';
        $instance['instagram']    = isset( $new_instance['instagram'] ) ? wp_strip_all_tags( $new_instance['instagram'] ) : '';
        $instance['youtube']    = isset( $new_instance['youtube'] ) ? wp_strip_all_tags( $new_instance['youtube'] ) : '';
        $instance['linkedin']    = isset( $new_instance['linkedin'] ) ? wp_strip_all_tags( $new_instance['linkedin'] ) : '';
        $instance['email']    = isset( $new_instance['email'] ) ? wp_strip_all_tags( $new_instance['email'] ) : '';
        $instance['container_width']    = isset( $new_instance['container_width'] ) ? wp_strip_all_tags( $new_instance['container_width'] ) : '';
        return $instance;
	}

	// Display the widget
	public function widget( $args, $instance ) {

        global $iwPluginsURI;


        extract( $args );

        // Check the widget options
        $iw_title    = isset( $instance['iw_title'] ) ? $instance['iw_title'] : '';
        $whiteicons = ! empty( $instance['whiteicons'] ) ? $instance['whiteicons'] : false;
        $facebook     = isset( $instance['facebook'] ) ? $instance['facebook'] : '';
        $twitter = isset( $instance['twitter'] ) ?$instance['twitter'] : '';
        $instagram     = isset( $instance['instagram'] ) ? $instance['instagram'] : '';
        $youtube     = isset( $instance['youtube'] ) ? $instance['youtube'] : '';
        $linkedin     = isset( $instance['linkedin'] ) ? $instance['linkedin'] : '';
        $email     = isset( $instance['email'] ) ? $instance['email'] : '';
        $container_width     = isset( $instance['container_width'] ) ? $instance['container_width'] : '';
    
        $css_file = $whiteicons ? 'widget-styles-white.css' : 'widget-styles-purple.css';

        wp_register_style( 'iw_css', $iwPluginsURI . "css/$css_file", false, '1.0' );
        wp_enqueue_style( 'iw_css' );

        // WordPress core before_widget hook (always include )
        echo $before_widget;
    
        $width = $container_width ? " style='width: $container_width' " : '';
       // Display the widget
       echo "<div id='iw-social-icons' class='widget-text wp_widget_plugin_box'$width>";

            // Display widget title if defined
            if ( $iw_title ) {
                echo $before_title . $iw_title . $after_title;
            }
    
            // Display text field
            echo '<div id="iw-icon-wrapper">';

            if ( $facebook ) {
                echo "<a href='$facebook' target='_blank'>";
                include('icons/facebook.svg');
                echo "</a>";
            }

            if ( $twitter ) {
                echo "<a href='$twitter' target='_blank'>";
                include('icons/twitter.svg');
                echo "</a>";
            }
    
            if ( $instagram ) {
                echo "<a href='$instagram' target='_blank'>";
                include('icons/instagram.svg');
                echo "</a>";
            }
    
            if ( $youtube ) {
                echo "<a href='$youtube' target='_blank'>";
                include('icons/youtube.svg');
                echo "</a>";
            }
    
            if ( $linkedin ) {
                echo "<a href='$linkedin' target='_blank'>";
                include('icons/linkedin.svg');
                echo "</a>";
            }

            if ( $email ) {
                echo "<a href='mailto:$email' target='_blank'>";
                include('icons/email.svg');
                echo "</a>";
            }
        echo '</div></div>';
    
        // WordPress core after_widget hook (always include )
        echo $after_widget;
	}

}

// Register the widget
function my_register_iw_social_icons_widget() {
	register_widget( 'IW_Social_Icons_Widget' );
}
add_action( 'widgets_init', 'my_register_iw_social_icons_widget' );